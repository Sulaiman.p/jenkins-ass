*** Keywords ***
Verify product name and author
    [Arguments]        ${name}      ${author}
    categories_page.Click categories
    categories_page.Click categories name
    categories_page.Verify product name      ${name}
    categories_page.Verify product author     ${author}
    categories_page.Click to product
    categories_page.Click product name
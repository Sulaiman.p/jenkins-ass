*** Keywords ***
Login to browser
    [Arguments]        ${username}        ${password}
    login_page.Input username                   ${username}
    login_page.Input password                   ${password}
    login_page.Click to login
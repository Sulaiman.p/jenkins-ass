*** Variables ***
${menu_locator}                    xpath=//div//a[text()='categories']
${categories_name_locator}         xpath=//a[@href="listproducts.php?cat=1"]
${product_locator}                 xpath=//a[@href="product.php?pic=7"]
${product_name_locator}            xpath=//form/input[@value="add this picture to cart"]

*** Keywords ***
Click categories
    common.Wait and click  ${menu_locator}

Click categories name
    common.Wait and click  ${categories_name_locator}

Verify product name
    [Arguments]     ${name}
    SeleniumLibrary.Element text should be        xpath=//a/h3[contains(text(), '${name}')]    ${name}

Verify product author
    [Arguments]     ${author}
    SeleniumLibrary.Element text should be        xpath=//p/a[contains(text(), '${author}')]  ${author}

Click to product
    common.Wait and click  ${product_locator}

Click product name
    common.Wait and click  ${product_name_locator}
*** Variables ***
${input_locator_username}    xpath=//td/input[@name='uname']
${input_locator_password}    xpath=//td/input[@name='pass']
${btn_login_locator}    xpath=//input[@value="login"]

*** Keywords ***
Input username
    [Arguments]        ${username}
    common.Wait and input text    ${input_locator_username}    ${username}

Input password
    [Arguments]        ${password}
    common.Wait and input text    ${input_locator_password}    ${password}

Click to login
    SeleniumLibrary.Wait until page contains element    ${btn_login_locator}
    SeleniumLibrary.Click element             ${btn_login_locator}
*** Keywords ***
Open browser
    [Arguments]    ${url}    ${browser}
    SeleniumLibrary.Open browser        ${url}        ${browser}

Close browsers
    SeleniumLibrary.Close all browsers

Wait and input text
    [Arguments]     ${input_locator}    ${text}
    SeleniumLibrary.Wait until page contains element    ${input_locator}
    SeleniumLibrary.Input text    ${input_locator}    ${text}

Wait and click
    [Arguments]     ${input_locator}
    SeleniumLibrary.Wait until page contains element        ${input_locator}
    SeleniumLibrary.Click Element        ${input_locator}